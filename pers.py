from config import *
import pygame
import math
import random
import time

class Pers(pygame.sprite.Sprite):
    def __init__(self,x,y,delta_x,delta_y,frame_len,select,position, color):
        pygame.sprite.Sprite.__init__(self)
        self.x = x
        self.y = y
        self.color = color
        self.select = select
        self.frame_len = frame_len
        self.delta_x = delta_x
        self.delta_y = delta_y
        self.position = False
        self.image = pygame.Surface((square_len - 2 * self.frame_len,square_len - 2 * self.frame_len))
        self.image.fill(pygame.Color(color))
        self.rect = pygame.Rect(x,y,square_len,square_len)
        self.length_of_going = 0
        self.frames = []

    def mousepos(self,mousex, mousey):
        self.position = False
        for i in range(self.x, self.x + square_len):
            for j in range(self.y, self.y + square_len):
                if (mousex,mousey) == (i,j) :
                    self.position = True
        self.select = self.position
    def update(self):
        self.image = pygame.Surface((square_len - 2 * self.frame_len,square_len - 2 * self.frame_len))
        self.image.fill(pygame.Color(self.color))
        self.x = self.x // square_len * square_len + self.delta_x
        self.y = self.y // square_len * square_len + self.delta_y
        self.rect.x = self.rect.x // square_len * square_len + self.delta_x
        self.rect.y = self.rect.y // square_len * square_len + self.delta_y
    def click(self):
        if (self.position == False):
            self.select = False
            self.frame_len = 0
        if (self.position == True and self.frame_len == 0):
            self.frame_len = 5
            self.select = True
        elif (self.position == True and self.frame_len > 0):
            self.frame_len = 0
            self.select = False
        self.create_going_zone()
        
        return self.select, self.frame_len
    def warp(self, x, y):
        if (math.fabs(self.x - x)/square_len*square_len + math.fabs(self.y - y)/square_len*square_len) <= (self.length_of_going+1) * (square_len):
            self.x = x
            self.y = y
            self.rect.x = x
            self.rect.y = y
        else:
            select == False
    def draw(self,screen):
        screen.blit(self.image,(self.rect.x + self.frame_len,self.rect.y + self.frame_len))
        surface = pygame.Surface((square_len, square_len))
        surface.fill(pygame.Color('orange'))
        for (x, y) in self.frames:
            screen.blit(surface, (x, y))

    def create_going_zone(self):
        if self.select:
            self.going_zone(self.length_of_going, self.rect.x, self.rect.y)
        else:
            self.frames = []

    def going_zone(self, length_of_going, x, y):
        self.frames.append((x, y))
        if length_of_going > 0:
            new_x = x + 1 * square_len
            self.going_zone(length_of_going - 1, new_x, y)
        if length_of_going > 0:
            new_y = y + 1 * square_len
            self.going_zone(length_of_going - 1, x, new_y)
        if length_of_going > 0:
            new_x = x - 1 * square_len
            self.going_zone(length_of_going - 1, new_x, y)
        if length_of_going > 0:
            new_y = y - 1 * square_len
            self.going_zone(length_of_going - 1, x, new_y)

            
        '''
        b = 0
        while self.length_of_going - b > 0:
            if self.select == True:
                b += 1
                for i in range (0, self.length_of_going - 1):
                    selecter = Selecter(self.x + (self.length_of_going - i) * square_len ,self.y + self.length_of_going  ,delta_x,delta_y,frame_len,select,position, 2, "orange")
                    selecter = Selecter(self.x ,self.y + (self.length_of_going - i) * square_len  ,delta_x,delta_y,frame_len,select,position, 2, "orange")
                    selecter = Selecter(self.x - (self.length_of_going - i) * square_len ,self.y + self.length_of_going  ,delta_x,delta_y,frame_len,select,position, 2, "orange")
                    selecter = Selecter(self.x  ,self.y + (self.length_of_going - i) * square_len  ,delta_x,delta_y,frame_len,select,position, 2, "orange")
                    self.draw(screen)
                self.going_zone(self, (self.length_of_going - b))
        '''     
                
            
                
class Scout(Pers):
    def __init__(self,x,y,delta_x,delta_y,frame_len,select,position, length_of_going, color):
        Pers.__init__(self,x,y,delta_x,delta_y,frame_len,select,position, color)
        self.length_of_going = length_of_going
        self.image.fill(pygame.Color(self.color))

class Settler(Pers):
    def __init__(self,x,y,delta_x,delta_y,frame_len,select,position, length_of_going, color):
        Pers.__init__(self,x,y,delta_x,delta_y,frame_len,select,position, color)
        self.length_of_going = length_of_going
        self.image.fill(pygame.Color(self.color))

class Selecter(Pers):
    def __init__(self,x,y,delta_x,delta_y,frame_len,select,position, length_of_going, color):
        Pers.__init__(self,x,y,delta_x,delta_y,frame_len,select,position, color)
        self.length_of_going = length_of_going
        self.image.fill(pygame.Color(self.color))
