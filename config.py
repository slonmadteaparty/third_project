screen_w, screen_h = 1200, 1200
square_len = 30
frame_len  = 0

delta_x = 0
delta_y = 0

water_percent = 0.2
ground_percent = 0.45
mountain_percent = 0.05
forest_percent = 0.3

water = '#0000aa'
ground = '#00aa00'
forest = '#003300'
mountain = '#9d9d9d'

material_priority = [water_percent, ground_percent, mountain_percent, forest_percent]
material_priority.sort()
material = {water_percent:water, ground_percent:ground, mountain_percent:mountain, forest_percent:forest}

select = False
position = False
