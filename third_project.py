import pygame
import random
import time
import math
from config import *
from pers   import Pers, Scout, Settler, Selecter

def genPercent():
    percent = [0]
    s = 0
    for i in material_priority:
        percent.append(s)
        s+=i
    percent.append(1)
    return percent

def setScreen():
    pygame.init()
    screen = pygame.display.set_mode((screen_w, screen_h))
    pygame.display.set_caption('Civilization VII')
    bg = pygame.Surface((screen_w, screen_h))
    bg.fill(pygame.Color(water)) 
    screen.blit(bg, (0, 0))
    return screen

def getColor():
    color = random.random()
    percent = genPercent()
    color_chosen = False
    right_material = 1
    while not color_chosen:
        #print(percent[right_material-1], color, percent[right_material])
        if color > percent[right_material-1] and color < percent[right_material]:
            color = material[material_priority[right_material-2]]
            color_chosen = True
        right_material += 1
    return color

def genMap():
    map_ = []
    for i in xrange(screen_w//square_len + 1):
        map_.append([])
        for j in xrange(screen_h//square_len + 1):
            surface = pygame.Surface((square_len, square_len))
            color = getColor()
            surface.fill(pygame.Color(color))
            map_[i].append(surface)
    return map_

def placeMap(screen, map_):
    for i in xrange(screen_w//square_len + 1):
        for j in xrange(screen_h//square_len + 1):
            screen.blit(map_[i][j], (i*square_len, j*square_len))

def updateScreen():
    pygame.display.update()

def testEvents(scout,settler,delta_x,delta_y):
    global in_loop
    delta_x,delta_y = 0,0
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            in_loop = False
            pygame.display.quit()
            continue
        
        if (event.type == pygame.KEYDOWN and event.key == pygame.K_RIGHT and scout.select == True):
            delta_x = square_len
        elif (event.type == pygame.KEYDOWN and event.key == pygame.K_LEFT and scout.select == True):
            delta_x = - square_len
        elif (event.type == pygame.KEYDOWN and event.key == pygame.K_UP and scout.select == True):
            delta_y = - square_len
        elif (event.type == pygame.KEYDOWN and event.key == pygame.K_DOWN and scout.select == True):
            delta_y = square_len
            
        if (event.type == pygame.MOUSEBUTTONDOWN and event.button == 1):
            scout.mousepos(event.pos[0], event.pos[1])
            select,frame_len = scout.click()
        if (event.type == pygame.MOUSEBUTTONDOWN and event.button == 3 and scout.select == True):
            scout.warp(*event.pos)
            
        if (event.type == pygame.MOUSEBUTTONDOWN and event.button == 1):
            settler.mousepos(event.pos[0], event.pos[1])
            select,frame_len = settler.click()
        if (event.type == pygame.MOUSEBUTTONDOWN and event.button == 3 and settler.select == True):
            settler.warp(*event.pos)

    return delta_x,delta_y




def main():
    global in_loop
    screen = setScreen()
    map_ = genMap()
    scout = Scout(square_len,square_len,delta_x,delta_y,frame_len,select,position, 5, "red")
    settler = Settler(square_len * 2,square_len * 2,delta_x,delta_y,frame_len,select,position, 2, "white")
    in_loop = True
    while in_loop:
        placeMap(screen, map_)
        scout.delta_x,scout.delta_y = testEvents(scout,settler,delta_x,delta_y)
        scout.update()
        scout.draw(screen)
        settler.update()
        settler.draw(screen)
        updateScreen()
main()
 
